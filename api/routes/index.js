const Router = require("express").Router();
const reqController = require("../controllers/reqController")

Router.use("/crimes", reqController);

module.exports = Router;
