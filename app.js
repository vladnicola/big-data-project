$(document).ready(function () {

  require("./ui-listeners.js");
  const {
    getData,
    getCluster,
    getInitialData,
  } = require("./services/requestService");
  const { loadClusterMap, loadHeatMap } = require("./services/loadMap");
  const Supercluster = require("supercluster");

  window.setVisible = (selector, visible) => {
    document.querySelector(selector).style.display = visible ? "block" : "none";
  };

  // Initialize leaflet.js
  const L = require("leaflet");
  require('./services/heatmap.js')(L);
  window.L = L;

  // Initialize the map
  window.map = L.map("map", {
    scrollWheelZoom: true,
  });

  // Set the position and zoom level of the map
  window.map.setView([40.73, -73.93], 11);

  // Initialize the base layer
  const osm_mapnik = L.tileLayer(
    "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
    {
      maxZoom: 19,
      attribution:
        '&copy; OSM Mapnik <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
    }
  ).addTo(map);

  function createClusterIcon(feature, latlng) {
    if (!feature.properties.cluster) {
      const marker = L.marker(latlng);
      marker.bindPopup(`
        <p><strong>CRIME DATE: </strong>${new Date(feature.properties.CRIME_DATE).toDateString()}</p>
        <p><strong>CRIME BOROUGH: </strong>${feature.properties.CRIME_DISTRICT}</p>
        <p><strong>LAW CATEGORY: </strong>${feature.properties.LAW_CATEGORY}</p>
        <p><strong>LOCATION: </strong>${feature.properties.CRIME_TYPE}</p>
        <p><strong>STATUS: </strong>${feature.properties.CRIME_STATUS}</p>
        <p><strong>DESCRIPTION: </strong>${feature.properties.CRIME_CATEGORY}</p>
      `)
      return marker;
    }
    const count = feature.properties.point_count;
    const size = count < 100 ? "small" : count < 1000 ? "medium" : "large";
    const icon = L.divIcon({
      html:
        "<div><span>" +
        feature.properties.point_count_abbreviated +
        "</span></div>",
      className: "marker-cluster marker-cluster-" + size,
      iconSize: L.point(40, 40),
    });
    return L.marker(latlng, {
      icon: icon,
    });
  }

  // Empty Layer Group that will receive the clusters data on the fly.
  window.markers = L.geoJSON(null, {
    pointToLayer: createClusterIcon,
  }).addTo(map);

  // Initial cluster map
  window.setVisible("#loading", true);
  getInitialData().then((crimes) => {
    window.loadData = new Supercluster({ radius: 80, maxZoom: 16 }).load(
      crimes
    );

    // Zoom to expand the cluster clicked by user.
    window.markers.on('click', (e) => {
      const clusterId = e.layer.feature.properties.cluster_id;
      const center = e.latlng;
      let expansionZoom;
      if (clusterId) {
        expansionZoom = window.loadData.getClusterExpansionZoom(clusterId);
        window.map.flyTo(center, expansionZoom);
      }
    });

    if (!window.isHeatmap) {
      loadClusterMap(window.loadData);
    } else {
      loadHeatMap(window.loadData.points, L);
    }
  });

  // Update the displayed clusters after user pan / zoom.
  window.map.on("moveend", () => {
    if (!window.isHeatmap) {
      loadClusterMap(window.loadData);
    } else {
      loadHeatMap(window.loadData.points, L);
    }
  });
});
