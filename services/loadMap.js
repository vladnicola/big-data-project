const loadClusterMap = (data, type) => {
  if (type == "default") window.map.setView([40.73, -73.93], 10);
  window.markers.clearLayers();
  if (window.heatmap) {map.removeLayer(window.heatmap);}
  const bounds = window.map.getBounds();
  const bbox = [
    bounds.getWest(),
    bounds.getSouth(),
    bounds.getEast(),
    bounds.getNorth(),
  ];
  const zoom = window.map.getZoom();
  window.markers.addData(data.getClusters(bbox, zoom));
  window.setVisible("#loading", false);
};

const loadHeatMap = (data, L) => {
  window.markers.clearLayers();
  if (window.heatmap) {map.removeLayer(window.heatmap);}
  const locations = data.map(function(rat) {
    const location = [];
    location.push(rat.geometry.coordinates[1]);
    location.push(rat.geometry.coordinates[0]);
    location.push(0.5);
    return location; // e.g. [50.5, 30.5, 0.2], // lat, lng, intensity
  });
  window.heatmap = L.heatLayer(locations, { radius: 25 }).addTo(map);
  window.setVisible("#loading", false);
};

module.exports = {
  loadClusterMap,
  loadHeatMap,
};
