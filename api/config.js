const { getSecret } = require("docker-secret");
const config = {
  dbUrl: process.env.DB_URL
    ? process.env.DB_URL
    : "mongodb+srv://dbuser:dbuser@cluster0.ydnpy.mongodb.net/bigdata",
  port: process.env.EXPOSE_PORT ? process.env.EXPOSE_PORT : "3001",
};
module.exports = config;
