const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');

const bSchema = new mongoose.Schema({});
bSchema.plugin(mongoosePaginate);

const BigData = mongoose.model("bigdata", bSchema, "bigdata");

module.exports = BigData;
