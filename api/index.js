const express = require("express");
const routes = require("./routes");
require("./db/db");
const config = require("./config");
const app = express();
const cors = require("cors");
const port = config.port;
const { handleError } = require("./errors");
const service = new (require("./service"))();

app.use(cors());
app.use(express.json());

app.use("/api", function (req, res, next) {
  req.service = service;
  next();
});
app.use("/api", routes);
app.use(handleError);

service.initClusters("init").then(() => {
  app.listen(port, () => {
    console.log(`Server running on port ${port}`);
  });
});

// app.listen(port, () => {
//   console.log(`Server running on port ${port}`);
// });