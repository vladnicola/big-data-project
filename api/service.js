"use strict";

const Supercluster = require("supercluster");
const BigData = require("./models/BigData");
const batchSize = 10000;

module.exports = class Service {
  constructor() {}

  async _getAllData() {
    const crimes = [];
    let i = 0;
    while (1) {
      const data = await BigData.find({})
        .skip(i * batchSize)
        .limit(batchSize);
      if (data.length) {
        crimes.push(...data);
        i++;
      } else {
        return crimes;
      }
    }
  }

  async getByQuery(query) {
    const options = {
      page: query.page ? query.page : 1,
      limit: query.limit ? query.limit : 10000,
      collation: {
        locale: "en",
      },
    };

    let crimes = await BigData.paginate(JSON.parse(query.query), options);
    return crimes;
  }

  async initClusters(type, query) {
    const data =
      type == "init"
        ? await this._getAllData()
        : (await this.getByQuery(query)).docs;

    this.index = data.map((datum) => {
      datum = JSON.parse(JSON.stringify(datum));
      datum.LOCATION.coordinates.reverse();
      return {
        type: "Feature",
        properties: datum,
        geometry: datum["LOCATION"],
      };
    });

    // Save all data
    if (type == "init") {
      this.initialData = this.index;
    }
  }

  getClusters(bbox, zoom) {
    let clusters = new Supercluster({ radius: 40, maxZoom: 16 }).load(
      this.index
    );
    return clusters.getClusters(bbox, zoom);
  }
};
