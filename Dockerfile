FROM node:14-alpine

WORKDIR /usr/src/app

COPY ./package*.json ./

RUN npm ci

EXPOSE 9966

CMD ["npm", "run","start"]