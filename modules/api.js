const axios = require("axios");
const api = axios.create({
  baseURL: "http://localhost:3001/api",
  timeout: 500000,
});

module.exports = api;
