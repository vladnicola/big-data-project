const {
  getData,
  getInitialData,
} = require("./services/requestService");

const { loadClusterMap, loadHeatMap } = require("./services/loadMap");


var startDate;
var endDate;

var applyButton = document.getElementById("apply");
const handleFilter = async () => {
  window.setVisible("#loading", true);
  let query = {
    $and: [],
  };

  // Update borough query
  let borough = $("#borough_selector").val();
  if (borough !== "all_boroughs") {
    query.$and.push({ CRIME_DISTRICT: borough });
  }

  // Update crime category query
  let category = $("#crime_selector").val();
  if (category !== "all_categories") {
    query.$and.push({ LAW_CATEGORY: category });
  }

  // Update crime type query
  var crimeType = [];
  $("input[name=crime_type]:checked").map(function () {
    crimeType.push($(this).val());
  });
  if (crimeType.length == 1) {
    if (crimeType[0] == "INSIDE") query.$and.push({ CRIME_TYPE: "INSIDE" });
    else {
      query.$and.push({ CRIME_TYPE: { $ne: "INSIDE" } });
    }
  }

  // Update crime status query
  var crimeStatus = [];
  $("input[name=crimeStatus]:checked").map(function () {
    crimeStatus.push($(this).val());
  });
  if (crimeStatus.length > 0) {
    query.$and.push({ CRIME_STATUS: { $in: crimeStatus } });
  }

  // Category search input
  let searchText = $("#category_search").val();
  if (searchText) {
    query.$and.push({
      CRIME_CATEGORY: { $regex: `.*${searchText}.*`, $options: "i" },
    });
  }

  // Date slide
  if(startDate&&endDate) {
    query.$and.push({
      CRIME_DATE: {
        $gte: new Date(startDate),
        $lt: new Date(endDate),
      },
    });
  }

  // Search data
  let crimes;
  if (query.$and.length == 0) {
    crimes = await getInitialData();
  } else {
    crimes = await getData(JSON.stringify(query), 2000);
  }
  window.loadData = new Supercluster({ radius: 80, maxZoom: 16 }).load(crimes);

  // Zoom to expand the cluster clicked by user.
  window.markers.on("click", (e) => {
    const clusterId = e.layer.feature.properties.cluster_id;
    const center = e.latlng;
    let expansionZoom;
    if (clusterId) {
      expansionZoom = window.loadData.getClusterExpansionZoom(clusterId);
      window.map.flyTo(center, expansionZoom);
    }
  });

  if (!window.isHeatmap) {
    loadClusterMap(window.loadData, "default");
  } else {
    loadHeatMap(window.loadData.points, window.L);
  }
  window.setVisible("#loading", false);
};

applyButton.addEventListener("click", handleFilter);


var inside_label = document.getElementById("inside_label");
var inside_button = document.getElementById("inside_button");
var outside_label = document.getElementById("outside_label");
var outside_button = document.getElementById("outside_button");

var attempted_label = document.getElementById("attempted_label");
var attempted_button = document.getElementById("attempted_button");
var completed_label = document.getElementById("completed_label");
var completed_button = document.getElementById("completed_button");

var points_label = document.getElementById("points_label");
var points_button = document.getElementById("points_button");
var heatmap_label = document.getElementById("heatmap_label");
var heatmap_button = document.getElementById("heatmap_button");

// var period_slider = document.getElementById("period_slider");
// var period_label = document.getElementById("period_label");

inside_label.addEventListener("click", inside_clicked);
outside_label.addEventListener("click", outside_clicked);

attempted_label.addEventListener("click", attempted_clicked);
completed_label.addEventListener("click", completed_clicked);

points_label.addEventListener("click", points_clicked);
heatmap_label.addEventListener("click", heatmap_clicked);

// period_slider.addEventListener("change", slider_changed);

// let months = [
//   "January",
//   "February",
//   "March",
//   "Aprill",
//   "May",
//   "June",
//   "July",
//   "August",
//   "September",
//   "October",
//   "November",
//   "December",
// ];
// let years = ["2013", "2014","2015"];
// let slider_text = ["All"];

// initSliderText();
// function initSliderText() {
//   for (let i = 0; i < years.length; i++) {
//     for (let j = 0; j < months.length; j++) {
//       slider_text.push(years[i] + " " + months[j]);
//     }
//   }
// }

// function slider_changed() {
//   let x = period_slider.value;
//   period_label.innerHTML = slider_text[x];
//   setTimeout(() => {
//     handleFilter();
//   }, 500);
// }

// function getRangeDate(label) {
//   if (label == "All") return 0;
//   let yearMonth = label.split(" ");

//   let year = yearMonth[0];
//   let month = months.indexOf(yearMonth[1]);

//   // Get first date of month
//   let firstDate = new Date(new Date(year, month, 2));
//   // Get end date of month
//   let endDate = new Date(new Date(year, month + 1, 1));

//   return { firstDate, endDate };
// }

function inside_clicked() {
  inside_button.checked = !inside_button.checked;
}

function outside_clicked() {
  outside_button.checked = !outside_button.checked;
}

function attempted_clicked() {
  attempted_button.checked = !attempted_button.checked;
}

function completed_clicked() {
  completed_button.checked = !completed_button.checked;
}

function points_clicked() {
  points_button.checked = true;
  heatmap_button.checked = false;
  window.isHeatmap = false;
  loadClusterMap(window.loadData);
}

function heatmap_clicked() {
  points_button.checked = false;
  heatmap_button.checked = true;
  window.isHeatmap = true;
  loadHeatMap(window.loadData.points, window.L);
}

// function search(input) {
//   if (event.key === "Enter") {
//     alert(input.value);
//     input.value = "";
//     input.blur();
//   }
// }


$('input[name="dates_range"]').daterangepicker({
  autoUpdateInput: false,
  showDropdowns: true,
  minYear: 2010,
  maxYear: 2021,
  alwaysShowCalendars:false,
  locale: {
      cancelLabel: 'Clear'
  }
});

$('input[name="dates_range"]').on('apply.daterangepicker', function(ev, picker) {
  $(this).val(picker.startDate.format('YYYY-MM-DD') + ' ~ ' + picker.endDate.format('YYYY-MM-DD'));
  startDate = picker.startDate.format('YYYY-MM-DD');
  endDate = picker.endDate.format('YYYY-MM-DD');
});

$('input[name="dates_range"]').on('cancel.daterangepicker', function(ev, picker) {
  $(this).val('');
  startDate = undefined;
  endDate = undefined;
});