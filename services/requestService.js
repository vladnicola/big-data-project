const api = require("../modules/api");

const getData = async (query, limit) => {
  // Count rows
  let total = await api.get("/crimes/count", {
    params: { query },
  });

  total = total.data.total;
  let pages = Math.ceil(total / limit);
  let requests = Array.from({ length: pages }, (_, i) => i + 1).map((i) => {
    return api.get("/crimes/query", {
      params: { page: i, limit: limit, query: query },
    });
  });

  const result = await Promise.all(requests);
  // Merge request result
  const data = result.map((el) => el.data);
  return data.flat(1);
};

const getCluster = async (bbox, zoom) => {
  let result = await api.post("/crimes/clusters", {
    bbox,
    zoom,
  });

  return result.data;
};

const getInitialData = async ()=>{
  let result = await api.get("/crimes/index");
  return result.data;
}

module.exports = {
  getData,
  getCluster,
  getInitialData
};
