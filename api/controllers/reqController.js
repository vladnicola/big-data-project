const express = require("express");
const BigData = require("../models/BigData");
const router = express.Router();

/**
 * Get Crime Information
 */
router.get("/", async (req, res, next) => {
  try {
    let query = req.query;
    const options = {
      page: query.page ? query.page : 1,
      limit: query.limit ? query.limit : 10000,
      collation: {
        locale: "en",
      },
    };

    ["page", "limit"].forEach(function (k) {
      delete query[k];
    });

    let crimes = await BigData.paginate(
      {
        ...query,
      },
      options
    );
    res.send(crimes);
  } catch (error) {
    next(error);
  }
});

/**
 * Get by mongo query string
 */
router.get("/query", async (req, res, next) => {
  try {
    await req.service.initClusters("update", req.query);
    res.send(req.service.index);
  } catch (error) {
    next(error);
  }
});

/**
 * Count
 */
router.get("/count", async (req, res, next) => {
  try {
    let nr = await BigData.find(JSON.parse(req.query.query)).count();
    res.send({ total: nr });
  } catch (error) {
    next(error);
  }
});

/**
 * Get initial data
 */
router.get("/index", async (req, res, next) => {
  try {
    res.send(req.service.initialData);
  } catch (error) {
    next(error);
  }
});

router.post("/clusters", async (req, res, next) => {
  try {
    res.send(req.service.getClusters(req.body.bbox, req.body.zoom));
  } catch (error) {
    next(error);
  }
});

module.exports = router;
